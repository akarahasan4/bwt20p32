function ucitajSortirano(dan, atribut, callback) {
  let formatiraniDan = dan ? dan : "";

  $.ajax({
    url: `http://localhost:8000/raspored/${formatiraniDan}?sort=${atribut}`,
    method: "GET",
    success: (data) => callback(data, null),
    error: (error) => callback(null, error),
  });
}
