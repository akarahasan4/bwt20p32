var izlaz = document.getElementById("izlaz");
var trebaBrisati = false;

function ispisiTabelu(data, tabelaId) {
  var tableBody = document.getElementsByTagName("tbody")[tabelaId - 1];

  var html = "";
  data.forEach((element) => {
    html += "<tr>";

    if (typeof element == "object")

      Object.values(element).forEach((x) => {
        html += "<td>" + x + "</td>";
      });

    else html += "<td>" + element + "</td>";

    html += "</tr>";
  });

  tableBody.innerHTML = html;
}

function ispisiAktivnosti(data, tabelaId) {
  var tableBody = document.getElementsByTagName("tbody")[tabelaId - 1];
  
  if(!data) return;

  var html = "";
  data.forEach((element) => {
    html += "<tr>";

    console.log(element)
    html += "<td>" + element.predmet.naziv  + " " + element.grupa.naziv + "</td>";
    html += "<td>" + element.naziv + " " + element.tip.naziv + "</td>";
    html += "<td>" + element.dan.naziv + "</td>";
    html += "<td>" + element.pocetak + "</td>";
    html += "<td>" + element.kraj + "</td>";


    html += "</tr>";
  });

  tableBody.innerHTML = html;
}

function ucitajListuAktivnosti() {
  $.ajax({
    url: `http://localhost:8000/v2/aktivnosti`,
    method: "GET",
    success: (data) => ispisiAktivnosti(data, 1),
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri učitavanju aktivnosti: ${error.responseJSON}`;
    },
  });
}

function ucitajListuPredmeta() {
  $.ajax({
    url: `http://localhost:8000/v2/predmet`,
    method: "GET",
    success: (data) => ispisiTabelu(data,2)
    ,
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
    },
  });
}

function ucitajListuDana() {
    $.ajax({
      url: `http://localhost:8000/v2/dan`,
      method: "GET",
      success: (data) => {
        data.forEach(dan => {
            var option = document.createElement("option");

            option.text = dan.naziv;
            option.value = dan.id;

            document.getElementById("dan-select").appendChild(option)
        });
      },
      error: (error) => {
        izlaz.className = "";
        izlaz.className = "bad";
        izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
      },
    });
  }

  function ucitajListuGrupa() {
    $.ajax({
      url: `http://localhost:8000/v2/grupa`,
      method: "GET",
      success: (data) => {
        data.forEach(dan => {
            var option = document.createElement("option");

            option.text = dan.naziv;
            option.value = dan.id;

            document.getElementById("grupa-select").appendChild(option)
        });
      },
      error: (error) => {
        izlaz.className = "";
        izlaz.className = "bad";
        izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
      },
    });
  }

function ucitajListuTipovaAktivnosti() {
$.ajax({
    url: `http://localhost:8000/v2/tip`,
    method: "GET",
    success: (data) => {
    data.forEach(tip => {
        var option = document.createElement("option");

        option.text = tip.naziv;
        option.value = tip.id;

        document.getElementById("aktivnost-tip-select").appendChild(option)
    });
    },
    error: (error) => {
    izlaz.className = "";
    izlaz.className = "bad";
    izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
    },
});
}

// PREDMET

function provjeriDostupnostPredmeta() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    method: "GET",
    url: `http://localhost:8000/v2/predmet/trazi?naziv=${naziv}`,
    success: (data) => {
      if (data == null) {
        trebaBrisati = true;
        dodajPredmet(naziv);
      } else {
        trebaBrisati = false;
        posaljiFormu();
      }
    },
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Doslo je do greske pri ucitavanju predmeta: ${error.responseJSON}`;
    },
  });
}

function nadjiPredmet() {
  return new Promise(function (resolve,reject) {
    var naziv = document.getElementById("predmet").value;
    
    $.ajax({
      method: "GET",
      url: `http://localhost:8000/v2/predmet/trazi?naziv=${naziv}`,
      success: (data) => { resolve(data);
      },
      error: (error) => {
        reject(error);
      },
    });
  })
}

function dodajPredmet() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    url: `http://localhost:8000/v2/predmet`,
    method: "POST",
    contentType: "application/x-www-form-urlencoded",
    data: {
      naziv: naziv,
    },
    success: () => posaljiFormu(),
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri dodavanju predmeta: ${error.responseJSON}`;
    },
  });
}

function izbrisiPredmet() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    url: `http://localhost:8000/v2/predmet?naziv=${naziv}`,
    method: "DELETE",
    contentType: "application/x-www-form-urlencoded",
    data: {
      naziv: naziv,
    },
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri brisanju predmeta: ${error.responseJSON}`;
    },
  });
}

// AKTIVNOST

async function posaljiFormu(noviPredmet) {
  var naziv = document.getElementById("predmet").value;
  var nazivAkt = document.getElementById("aktinvost-naziv").value;
  var aktivnost = document.getElementById("aktivnost-tip-select").value;
  var dan = document.getElementById("dan-select").value;
  var start = document.getElementById("pocetak").value;
  var end = document.getElementById("kraj").value;
  
  nadjiPredmet(naziv).then(predmetid => {  
    console.log(predmetid)
    $.ajax({
      method: "POST",
      url: `http://localhost:8000/v2/aktivnosti`,
      contentType: "application/x-www-form-urlencoded",
      data: {
        naziv: nazivAkt,
        tipId: aktivnost,
        danId: dan,
        pocetak: start,
        kraj: end,
        predmetId:predmetid.id,
        grupaId:document.getElementById("grupa-select").value
      },
      success: () => {
        ucitajListuAktivnosti();
        ucitajListuPredmeta();
        izlaz.className = "";
        izlaz.className = "good";
        izlaz.innerHTML = "Aktivnost uspješno dodana.";
      },
      error: (error) => {
        if (trebaBrisati) izbrisiPredmet();
        izlaz.className = "";
        izlaz.className = "bad";
        izlaz.innerHTML = `Doslo je do greske pri ucitavanju aktivnosti: ${error.responseJSON}`;
      },
    });
  });
  }
  
ucitajListuAktivnosti();
ucitajListuPredmeta();
ucitajListuDana();
ucitajListuTipovaAktivnosti();
ucitajListuGrupa();