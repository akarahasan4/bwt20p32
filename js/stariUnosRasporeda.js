var izlaz = document.getElementById("izlaz");
var trebaBrisati = false;

function ispisiTabelu(data, tabelaId) {
  var tableBody = document.getElementsByTagName("tbody")[tabelaId - 1];

  var html = "";
  data.forEach((element) => {
    html += "<tr>";

    if (typeof element == "object")
      Object.values(element).forEach((x) => {
        html += "<td>" + x + "</td>";
      });
    else html += "<td>" + element + "</td>";

    html += "</tr>";
  });

  tableBody.innerHTML = html;
}

function ucitajListuAktivnosti() {
  $.ajax({
    url: `http://localhost:8000/raspored`,
    method: "GET",
    success: (data) => ispisiTabelu(data, 1),
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri učitavanju aktivnosti: ${error.responseJSON}`;
    },
  });
}

function ucitajListuPredmeta() {
  $.ajax({
    url: `http://localhost:8000/predmeti`,
    method: "GET",
    success: (data) => ispisiTabelu(data, 2),
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
    },
  });
}

// PREDMET

function provjeriDostupnostPredmeta() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    method: "GET",
    url: `http://localhost:8000/predmet/${naziv}`,
    success: (data) => {
      if (data == null) {
        trebaBrisati = true;
        dodajPredmet(naziv);
      } else {
        trebaBrisati = false;
        posaljiFormu();
      }
    },
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Doslo je do greske pri ucitavanju predmeta: ${error.responseJSON}`;
    },
  });
}

function dodajPredmet() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    url: `http://localhost:8000/predmet`,
    method: "POST",
    contentType: "application/x-www-form-urlencoded",
    data: {
      naziv: naziv,
    },
    success: () => posaljiFormu(),
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri dodavanju predmeta: ${error.responseJSON}`;
    },
  });
}

function izbrisiPredmet() {
  var naziv = document.getElementById("predmet").value;

  $.ajax({
    url: `http://localhost:8000/predmet/${naziv}`,
    method: "DELETE",
    contentType: "application/x-www-form-urlencoded",
    data: {
      naziv: naziv,
    },
    error: (error) => {
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Došlo je do greške pri brisanju predmeta: ${error.responseJSON}`;
    },
  });
}

// AKTIVNOST

function posaljiFormu() {
  var naziv = document.getElementById("predmet").value;
  var aktivnost = document.getElementById("aktivnost").value;
  var dan = document.getElementById("dan").value;
  var start = document.getElementById("pocetak").value;
  var end = document.getElementById("kraj").value;

  $.ajax({
    method: "POST",
    url: `http://localhost:8000/raspored`,
    contentType: "application/x-www-form-urlencoded",
    data: {
      predmet: naziv,
      aktivnost: aktivnost,
      dan: dan,
      pocetak: start,
      kraj: end,
    },
    success: () => {
      ucitajListuAktivnosti();
      ucitajListuPredmeta();
      izlaz.className = "";
      izlaz.className = "good";
      izlaz.innerHTML = "Aktivnost uspješno dodana.";
    },
    error: (error) => {
      if (trebaBrisati) izbrisiPredmet();
      izlaz.className = "";
      izlaz.className = "bad";
      izlaz.innerHTML = `Doslo je do greske pri ucitavanju aktivnosti: ${error.responseJSON}`;
    },
  });
}

ucitajListuAktivnosti();
ucitajListuPredmeta();
