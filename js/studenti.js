function posalji() {
    $.ajax({
       method:"POST",
       url:"http://localhost:8000/v2/student",
       data: {
           studenti:document.getElementById("studenti").value,
           grupa:document.getElementById("grupa-select").value
       },
       contentType: "application/x-www-form-urlencoded",
       success: (poruka) => {
           document.getElementById("studenti").value = poruka;
       },
       error:(poruka) => {
        document.getElementById("studenti").value = poruka;
    },
    })
}

function ucitajListuGrupa() {
    $.ajax({
      url: `http://localhost:8000/v2/grupa`,
      method: "GET",
      success: (data) => {
        data.forEach(dan => {
            var option = document.createElement("option");

            option.text = dan.naziv;
            option.value = dan.id;

            document.getElementById("grupa-select").appendChild(option)
        });
      },
      error: (error) => {
        izlaz.className = "";
        izlaz.className = "bad";
        izlaz.innerHTML = `Došlo je do greške pri učitavanju predmeta: ${error.responseJSON}`;
      },
    });
  }

  ucitajListuGrupa();