var atribut = "predmet";
var smjer = "A";
var dan = "";
var poruka = document.getElementById("poruka");

function ispisiRezultat(data, error) {
  var greskaHTML = "Doslo je do greske, ";
  greskaHTML += error;

  if (error) document.getElementById("izlaz").innerHTML = greskaHTML;
  else {
    var body = document.getElementsByTagName("tbody")[0];

    var html = "";
    data.forEach((element) => {
      html += "<tr>";
      Object.values(element).forEach((x) => {
        html += "<td>" + x + "</td>";
      });
      html += "</tr>";
    });
  }

  body.innerHTML = html;
}

document.getElementById("danSelect").addEventListener("change", (e) => {
  ucitajSortirano(e.target.value, `${smjer}predmet`, ispisiRezultat);
  dan = e.target.value;

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;
});

document.getElementById("head-predmet").addEventListener("click", (e) => {
  if (atribut == "predmet") smjer = smjer == "D" ? "A" : "D";
  else smjer = "A";
  atribut = "predmet";

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;

  ucitajSortirano(dan, `${smjer}predmet`, ispisiRezultat);
});

document.getElementById("head-aktivnost").addEventListener("click", (e) => {
  if (atribut == "aktivnost") smjer = smjer == "D" ? "A" : "D";
  else smjer = "A";
  atribut = "aktivnost";

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;

  ucitajSortirano(dan, `${smjer}aktivnost`, ispisiRezultat);
});

document.getElementById("head-dan").addEventListener("click", (e) => {
  if (atribut == "dan") smjer = smjer == "D" ? "A" : "D";
  else smjer = "A";
  atribut = "dan";

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;

  ucitajSortirano(dan, `${smjer}dan`, ispisiRezultat);
});

document.getElementById("head-pocetak").addEventListener("click", (e) => {
  if (atribut == "pocetak") smjer = smjer == "D" ? "A" : "D";
  else smjer = "A";
  atribut = "pocetak";

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;

  ucitajSortirano(dan, `${smjer}pocetak`, ispisiRezultat);
});

document.getElementById("head-kraj").addEventListener("click", (e) => {
  if (atribut == "kraj") smjer = smjer == "D" ? "A" : "D";
  else smjer = "A";
  atribut = "kraj";

  poruka.innerHTML = `
    Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
    Sortirano po: <span style="font-weight:bold">${atribut}</span> 
    u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
  `;

  ucitajSortirano(dan, `${smjer}kraj`, ispisiRezultat);
});

ucitajSortirano(dan, `${smjer}${atribut}`, ispisiRezultat);

poruka.innerHTML = `
  Prikaz za dan: <span style="font-weight:bold">${dan == "" ? "svi" : dan}</span>. 
  Sortirano po: <span style="font-weight:bold">${atribut}</span> 
  u <span style="font-weight:bold">${smjer == "A" ? "rastućem" : "opadajućem"}</span> poretku
`;
