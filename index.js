﻿const fs = require("fs");
const cors = require("cors");
const csv = require("csvtojson");
const express = require("express");
const bodyParser = require("body-parser");
const { json } = require("body-parser");

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static("html"));
app.use(express.static("css"));
app.use(express.static("js"));

var imenaDana = ["ponedjeljak", "utorak", "srijeda", "cetvrtak", "petak"];

var db = require("./baza/baza");
const { parse } = require("path");


// SPIRALA 4  

// CRUD Aktivnosti

function isFloat(n) {
  return n === +n && n !== (n|0);
}

function isInteger(n) {
  return n === +n && n === (n|0);
}

function novaValidacija(body) {
  if (!body.naziv || body.naziv == "") throw Error("Naziv aktivnosti nije unesen.");
  if(body.pocetak == "") throw Error("Vrijeme pocetka nije uneseno.");

  var poc = Number(body.pocetak)
  if(!isFloat(poc) && !isInteger(poc)) throw Error("Pocetak nije broj")

  var krj = Number(body.kraj)
  if(!isFloat(krj) && !isInteger(krj)) throw Error("Kraj nije broj")
}

app.get("/v2/aktivnosti", (req, res) => {
  db.aktivnosti.findAll({include:[db.predmet,db.grupa,db.tip,db.dan]}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/aktivnosti/trazi", (req, res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.aktivnosti.findOne({include:[db.predmet,db.grupa,db.tip,db.dan],where:where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/aktivnosti", (req, res) => {
  try {
    novaValidacija(req.body)

    db.aktivnosti.create({
      naziv:req.body.naziv,
      pocetak:parseFloat(req.body.pocetak),
      kraj:parseFloat(req.body.kraj),
      danId:req.body.danId,
      tipId:req.body.tipId,
      grupaId:req.body.grupaId,
      predmetId:req.body.predmetId
    }).then((results) => {
      res.json(results)
    }).catch(error => {
      res.status(400);
      res.json(error);
    })
  } catch(error) {
    res.status(400)
    res.json(error.message);
  }

});

app.put("/v2/aktivnosti", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.aktivnosti.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.update(req.body).then((result) => {
        res.json("Aktivnost modifikovana")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena aktivnost sa tim parametrima");
    }
  })
})

app.delete("/v2/aktivnosti", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.aktivnosti.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.destroy().then((result) => {
        res.json("Aktivnost obrisana")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena aktivnost sa tim parametrima");
    }
  })
})

// CRUD Dan

app.get("/v2/dan", (req, res) => {
  db.dan.findAll().then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/dan/trazi", (req, res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.dan.findOne({where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/dan", (req, res) => {
  db.dan.create({naziv:req.body.naziv}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.put("/v2/dan", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.dan.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.update(req.body).then((result) => {
        res.json("Dan modifikovan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen dan sa tim parametrima");
    }
  })
})

app.delete("/v2/dan", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.dan.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.destroy().then((result) => {
        res.json("Dan obrisan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen dan sa tim parametrima");
    }
  })
})

// CRUD Grupa

app.get("/v2/grupa", (req, res) => {
  db.grupa.findAll().then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/grupa/trazi", (req, res) => {
  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.grupa.findOne({where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/grupa", (req, res) => {
  db.grupa.create({ naziv:req.body.naziv}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.put("/v2/grupa", (req,res) => {
  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.grupa.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.update(req.body).then((result) => {
        res.json("Grupa modifikovana")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena grupa sa tim parametrima");
    }
  })
})

app.delete("/v2/grupa", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.grupa.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.destroy().then((result) => {
        res.json("Grupa obrisana")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena grupa sa tim parametrima");
    }
  })
})

// CRUD Predmet

app.get("/v2/predmet", (req, res) => {
  db.predmet.findAll({attributes:["naziv"]}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/predmet/trazi", (req, res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.predmet.findOne({attributes:["id","naziv"],where:where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/predmet", (req, res) => {
  db.predmet.create({ naziv:req.body.naziv }).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.put("/v2/predmet", (req,res) => {
  let where ={}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.predmet.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.update(req.body).then((result) => {
        res.json("Predmet modifikovan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena predmet sa tim parametrima");
    }
  })
})

app.delete("/v2/predmet", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.predmet.findOne({where}).then((pronadjena) => {
    if(pronadjena != null) {
      pronadjena.destroy().then((result) => {
        res.json("Predmet obrisan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjena predmet sa tim parametrima");
    }
  })
})

// CRUD Student

app.get("/v2/student", (req, res) => {
  db.student.findAll().then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/student/trazi", (req, res) => {
  let where = {}

  if(req.query.ime) {
    where = {ime:req.query.ime}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else if(req.query.index) {
    where = {index:req.query.index}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.student.findOne({where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/student",async (req, res) => {
  
  let parsirano = req.body.studenti.split("\n");

  let odgovor = []

  for(let i in parsirano) {
      x = parsirano[i];
      if(x != "" && x!= undefined && x!="\n") {
   
        let ime = x.split(",")[0]
        let index = x.split(",")[1]
        let pronadjeni = await db.student.findOne({where:{index:index},include:[db.grupa]});

        if(pronadjeni == null) {
          db.student.create({
            ime:ime,
            index:index,
          })
        } else if(pronadjeni.grupas.length == 0) {
          odgovor.push("Student " + ime + " nije kreiran jer postoji student " + pronadjeni.dataValues.ime +  " sa istim indexom " + index);
        } 
        
    } 
  } 

    res.json(odgovor);
});

app.put("/v2/student", (req,res) => {
  let where = {}

  if(req.query.ime) {
    where = {ime:req.query.ime}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else if(req.query.index) {
    where = {index:req.query.index}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.student.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.update(req.body).then((result) => {
        res.json("Student modifikovan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen student sa tim parametrima");
    }
  })
})

app.delete("/v2/student", (req,res) => {
  let where = {}

  if(req.query.ime) {
    where = {ime:req.query.ime}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else if(req.query.index) {
    where = {index:req.query.index}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.student.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.destroy().then((result) => {
        res.json("Student obrisan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen student sa tim parametrima");
    }
  })
})

// CRUD Tip

app.get("/v2/tip", (req, res) => {
  db.tip.findAll().then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.get("/v2/tip/trazi", (req, res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }
  
  db.tip.findOne({where}).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.post("/v2/tip", (req, res) => {
  db.tip.create({ naziv:req.body.naziv }).then((results) => {
    res.json(results)
  }).catch(error => {
    res.status(400);
    res.json(error);
  })
});

app.put("/v2/tip", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.tip.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.update(req.body).then((result) => {
        res.json("Tip modifikovan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen tip sa tim parametrima");
    }
  })
})

app.delete("/v2/tip", (req,res) => {
  let where = {}

  if(req.query.naziv) {
    where = {naziv:req.query.naziv}
  } else if(req.query.id) {
    where = {id:req.query.id}
  } else {
    res.status(400);
    res.json("Greska u parametrima");
    return;
  }

  db.tip.findOne({where}).then((pronadjeni) => {
    if(pronadjeni != null) {
      pronadjeni.destroy().then((result) => {
        res.json("Tip obrisan")
      }).catch(error => {
        console.log(error);
        res.json(error)
      })
    } else {
      res.json("Nije pronadjen tip sa tim parametrima");
    }
  })
})



// SPIRALA 3

function ConvertToCSV(objArray) {
  var array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
  var str = "";

  for (var i = 0; i < array.length; i++) {
    var line = "";
    for (var index in array[i]) {
      if (line != "") line += ",";

      line += array[i][index];
    }

    str += line + "\r\n";
  }

  return str;
}

// GET PREDMET

function ucitajPredmet(predmet) {
  var predmeti = fs.readFileSync("predmeti.csv").toString().split("\n");
  let trazeni = predmeti.filter((x) => x == predmet)[0];

  return trazeni;
}

function ucitajSvePredmete() {
  let predmeti = fs.readFileSync("predmeti.csv").toString().split("\n");
  if (predmeti[predmeti.length - 1] == "") predmeti.splice(predmeti.length - 1, 1);
  return predmeti;
}

app.get("/v1/predmeti", (req, res) => {
  res.json(ucitajSvePredmete());
});

app.get("/v1/predmet/:naziv", (req, res) => {
  let predmet = ucitajPredmet(req.params.naziv);
  res.json(predmet ? predmet : null);
});

// POST PREDMET

app.post("/v1/predmet", (req, res) => {
  if (ucitajPredmet(req.body.naziv)) res.json("Naziv predmeta postoji!");
  else {
    fs.appendFile("predmeti.csv", req.body.naziv + "\n", function (err) {
      if (err) throw err;
      console.log("Novi predmet uspješno dodan!");
    });
    res.json("Predmet uspješno dodan!");
  }
});

// DELETE PREDMET

app.delete("/v1/predmet/:naziv", (req, res) => {
  if (ucitajPredmet(req.params.naziv)) {
    let predmeti = ucitajSvePredmete();

    let novi = predmeti.filter((x) => x != req.params.naziv);

    let izlaz = "";

    novi.forEach((x) => {
      if (x != "") izlaz += x + "\n";
    });

    fs.writeFileSync("predmeti.csv", izlaz, () => {});

    res.json("Predmet uspješno obrisan");
    console.log("Predmet uspješno obrisan");
  }
});

// GET RASPORED

function ucitajCSVAktivnosti(putDoFajla) {
  const aktivnosti = fs.readFileSync(putDoFajla, { encoding: "utf8", flag: "r" }).toString();

  let parsirano = [];

  aktivnosti.split("\n").forEach((red) => {
    let vrijednosti = red.split(",");
    if (vrijednosti.length === 5) {
      parsirano.push({
        predmet: vrijednosti[0],
        aktivnost: vrijednosti[1],
        dan: vrijednosti[2],
        pocetak: vrijednosti[3],
        kraj: vrijednosti[4],
      });
    }
  });

  return parsirano;
}

app.get("/v1/raspored/:dan?", (req, res) => {
  fs.access("raspored.csv", fs.F_OK, (err) => {
    if (err) {
      res.json("Datoteka raspored.csv nije kreirana!");
      return;
    }

    const danParam = req.params.dan;
    const sortQuery = req.query.sort;

    let aktivnosti = ucitajCSVAktivnosti("raspored.csv");

    if (danParam != "svi" && danParam) aktivnosti = aktivnosti.filter((x) => x.dan == danParam);

    if (sortQuery) {
      let smjer = sortQuery[0];
      let atribut = sortQuery.substr(1);

      aktivnosti.sort((a, b) => {
        let prvi, drugi;

        if (atribut == "dan") {
          prvi = brojDanaString(a[atribut]);
          drugi = brojDanaString(b[atribut]);
        } else {
          prvi = a[atribut];
          drugi = b[atribut];
        }

        if (smjer == "A") return prvi.localeCompare(drugi);
        else return drugi.localeCompare(prvi);
      });
    }

    if (req.headers.accept.includes("text/csv")) {
      res.setHeader("Content-Type", "text/csv");
      res.end(ConvertToCSV(aktivnosti));
    } else {
      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify(aktivnosti));
    }
  });
});

// POST RASPORED

function brojDanaString(dan) {
  switch (dan) {
    case "ponedjeljak":
      return "1";
    case "utorak":
      return "2";
    case "srijeda":
      return "3";
    case "cetvrtak":
      return "4";
    case "petak":
      return "5";
  }
}

function validiraj(req) {
  if (!req.body.predmet || req.body.predmet == "") throw Error("Predmet nije unesen.");

  if (!req.body.aktivnost || req.body.aktivnost == "") throw Error("Aktivnost nije unesena.");

  if (
    req.body.aktivnost != "predavanje" &&
    req.body.aktivnost != "predavanja" &&
    req.body.aktivnost != "vježbe" &&
    req.body.aktivnost != "vjezbe" &&
    req.body.aktivnost != "vježba" &&
    req.body.aktivnost != "vjezba"
  )
    throw Error("Neispravna aktivnost.");

  if (!req.body.dan || req.body.dan == "") throw Error("Dan nije unesen.");

  if (!imenaDana.includes(req.body.dan.toLowerCase()))
    throw Error("Dan nije validan, validni dani " + imenaDana + "(dozvoljeno i malim slovima)");

  if (req.body.pocetak == "") throw Error("Vrijeme početka nije uneseno.");

  if (req.body.kraj == "") throw Error("Vrijeme kraja nije uneseno.");

  var reg = /(?:[01]\d|2[0123]):(?:[012345]\d)/;
  if (!req.body.pocetak.match(reg)) throw Error("Neispravno vrijeme pocetka.");

  if (!req.body.kraj.match(reg)) throw Error("Neispravno vrijeme kraja.");
}

app.post("/v1/raspored", (req, res) => {
  try {
    validiraj(req);

    var par = req.body;
    var nl = par.predmet + "," + par.aktivnost + "," + par.dan + "," + par.pocetak + "," + par.kraj;

    fs.appendFile("raspored.csv", nl + "\n", function (err) {
      if (err) throw err;
      console.log("Novi red uspješno dodan!");
      res.writeHead(200, {});
      res.end(nl.toString());
    });
  } catch (err) {
    res.status(400);
    res.json(err.message);
  }
});

 app.listen(8000, () => { // Port 8000 je zamjenjen u odnosu na prošlu spiralu jer je wamp učitavao svoje stranice umjesto mog htmla na portu 8080
  db.sequelize.sync({ force: true }).then(async () => {
    var rma = (await db.predmet.create({naziv:"RMA"})).dataValues.id;
    var wt = (await db.predmet.create({naziv:"WT"})).dataValues.id;
    var asp = (await db.predmet.create({naziv:"ASP"})).dataValues.id;
    var rmag1 = (await db.grupa.create({naziv:"RMAgrupa1",predmet:rma})).dataValues.id;
    var wtg1 = (await db.grupa.create({naziv:"WTgrupa1",predmetId:wt})).dataValues.id;
    var wtg2 = (await db.grupa.create({naziv:"WTgrupa2",predmetId:wt})).dataValues.id;
    var asp1 = (await db.grupa.create({naziv:"ASPgrupa1",predmetId:wt})).dataValues.id;
    var student1 = (await db.student.create({ime:"Neko Nekic",index:12345,grupa:wtg1})).dataValues.id;
    var student2 = (await db.student.create({ime:"Cetvrti Neko",index:18119,grupa:rmag1})).dataValues.id;
    var student3 = (await db.student.create({ime:"Treci Neko",index:18009,grupa:rmag1})).dataValues.id;
    var dan = (await db.dan.create({naziv:"Ponedjeljak"})).dataValues.id;
    var dan = (await db.dan.create({naziv:"Utorak"})).dataValues.id;
    var dan = (await db.dan.create({naziv:"Srijeda"})).dataValues.id;
    var dan = (await db.dan.create({naziv:"Cetvrtak"})).dataValues.id;
    var dan = (await db.dan.create({naziv:"Petak"})).dataValues.id;
    var tip = (await db.tip.create({naziv:"vjezbe"})).dataValues.id;
  });
})
