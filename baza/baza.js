const Sequelize = require("sequelize");
const db = {};

const sequelize = new Sequelize("wt2032-ST", "root", "", {
  host: "127.0.0.1",
  dialect: "mysql",
});

db.grupa = sequelize.import(__dirname + "/grupa.js");
db.predmet = sequelize.import(__dirname + "/predmet.js");
db.aktivnosti = sequelize.import(__dirname + "/aktivnosti.js");
db.dan = sequelize.import(__dirname + "/dan.js");
db.tip = sequelize.import(__dirname + "/tip.js");
db.student = sequelize.import(__dirname + "/student.js");
db.grupaStudenti = sequelize.import(__dirname + "/grupastudenti.js");


db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.grupa.belongsTo(db.predmet);
db.predmet.hasMany(db.grupa);
db.aktivnosti.belongsTo(db.predmet);
db.predmet.hasMany(db.aktivnosti);
db.aktivnosti.belongsTo(db.grupa);
db.grupa.hasMany(db.aktivnosti);
db.aktivnosti.belongsTo(db.dan);
db.dan.hasMany(db.aktivnosti);
db.aktivnosti.belongsTo(db.tip);
db.tip.hasMany(db.aktivnosti);
db.student.belongsToMany(db.grupa,{through:"grupastudentmedjutabela",allowNull:true});
db.grupa.belongsToMany(db.student,{through:"grupastudentmedjutabela",allowNull:true});

module.exports = db;
